#!/usr/bin/env python3
"""
 simple asyn example

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""


import asyncio

IDX = 0


async def talker(name: str, delay: float = 1.0):
    global IDX

    while True:
        print(f"{name} {IDX=}")
        IDX += 1
        await asyncio.sleep(delay)


async def main():
    async with asyncio.TaskGroup() as tg:
        for idx in range(1, 10):
            tg.create_task(talker(name=f"talker_{idx}"))


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
