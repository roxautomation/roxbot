#!/usr/bin/env python3
"""
 Example of using the gps simulator

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""
import asyncio
import math
import time

import coloredlogs

from roxbot.gps import Mock_GPS

coloredlogs.install(
    level="DEBUG", fmt="%(asctime)s,%(msecs)03d %(name)s %(levelname)s %(message)s"
)


GPS = Mock_GPS(nmea_out="/tmp/tty_nmea_tx")


async def set_pose():
    """set gps enu pose, simulates gps moving around in circle"""
    x = 0.0
    y = 0.0
    theta = 0.0
    t = time.time()

    radius = 20.0

    # 10 seconds =  1 revolution
    while True:
        x = math.sin(theta) * radius
        y = math.cos(theta) * radius
        t_elapsed = time.time() - t
        theta = t_elapsed * 2 * math.pi / 10
        GPS.set_pose(x, y, theta)
        await asyncio.sleep(0.1)


async def main():
    """main loop"""
    await asyncio.gather(GPS.main(1), set_pose())


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
