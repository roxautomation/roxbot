#!/bin/bash
set -e

trap 'kill 0' SIGINT
echo "Creating gps virtual ports..."

socat -d pty,rawer,b115200,echo=0,link=/tmp/tty_nmea_rx \
      pty,rawer,b115200,echo=0,link=/tmp/tty_nmea_tx &

./gps_sim.py

wait
