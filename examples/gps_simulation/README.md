# GPS Simulation

Demonstrate creation of virtual ports and generating of mock gps data

# how to use

1. run `./gps_demo.sh`
2. show nmea messages on serial with `picocom -b 115200 /tmp/tty_nmea_rx`
