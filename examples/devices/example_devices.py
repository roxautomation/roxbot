#!/usr/bin/env python3
"""
 Run a couple of simulated devices and a bridge.

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

import argparse
import asyncio
import logging
from typing import List

import coloredlogs

from roxbot import LOG_FORMAT
from roxbot.bridges.ws_bridge import WS_Bridge
from roxbot.hal.canopen import CanBus_Mock

# Set up argument parser
parser = argparse.ArgumentParser(description="Run the main server.")
parser.add_argument("--debug", action="store_true", help="Enable debug logging.")
args = parser.parse_args()


log = logging.getLogger(__name__)

# Set log level based on --debug option
log_level = "DEBUG" if args.debug else "INFO"
coloredlogs.install(level=log_level, fmt=LOG_FORMAT)


async def poll_devices(devices: List):
    """poll devices"""

    log.debug("%s", f"polling devices: {devices}")

    while True:
        for dev in devices:
            dio = dev.dio
            log.info("device %s: %s", dev.name, dio)

        await asyncio.sleep(1)


async def main():
    """main coroutine"""
    bridge = WS_Bridge()
    await bridge.serve()

    bus = CanBus_Mock(bridge=bridge)
    await bus.start()

    devices = bus.devices
    log.info("Bus devices: %s", devices)

    # start other tasks
    async with asyncio.TaskGroup() as tg:
        tg.create_task(poll_devices(devices))
        # add more tasks here ...


if __name__ == "__main__":
    # silence bridge
    logging.getLogger("ws_bridge").setLevel(logging.INFO)

    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("exiting")
