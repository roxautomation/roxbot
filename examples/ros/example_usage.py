#!/usr/bin/env python3
"""
 Example of using ROS_Bridge class to communicate with ROS through rosbridge protocol.

 Copyright (c) 2024 ROX Automation - Jev Kuznetsov
"""
import asyncio
import logging

import coloredlogs  # type: ignore

from roxbot.bridges import ROS_Bridge

coloredlogs.install(level="INFO", fmt="%(asctime)s %(levelname)s %(message)s")

logger = logging.getLogger("main")


async def listen(bridge: ROS_Bridge):
    """Listens to messages from a ROS topic using the ROS bridge."""
    logger.info("Listening to ROS topic...")
    async with bridge.subscriber("/signals/ramp") as sub:
        try:
            async for message in sub:
                logger.info(f"< {message}")
        except asyncio.CancelledError:
            logger.info("Cancelled listener")


async def talk(bridge: ROS_Bridge, n_runs: int = 3):
    """Sends messages to a ROS topic using the ROS bridge."""
    async with bridge.publisher("/bridged", "std_msgs/String") as pub:
        for idx in range(n_runs):
            await pub.publish({"data": f"Hello ROS! #{idx}"})
            await asyncio.sleep(1)


async def main():
    """Main function to demonstrate ROS bridge subscriber and publisher."""

    async with ROS_Bridge("ws://localhost:9090") as bridge:
        listen_task = asyncio.create_task(listen(bridge))

        await talk(bridge)

        listen_task.cancel()
        await listen_task


if __name__ == "__main__":
    asyncio.run(main())
