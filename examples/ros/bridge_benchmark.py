#!/usr/bin/env python3
"""
 Example of using ROS_Bridge class to communicate with ROS through rosbridge protocol.

 Copyright (c) 2024 ROX Automation - Jev Kuznetsov
"""
import asyncio
import logging
from time import perf_counter

import coloredlogs

from roxbot.bridges import ROS_Bridge

coloredlogs.install(level="INFO", fmt="%(asctime)s %(levelname)s %(message)s")

logger = logging.getLogger("main")


async def benchmark(n_runs: int = 1_000):
    """Benchmark the message rate."""

    async with ROS_Bridge("ws://localhost:9090") as bridge:
        logger.info("Benchmarking message rate...")
        start = perf_counter()

        async with bridge.publisher("/bench", "std_msgs/Int32") as pub:
            for idx in range(n_runs):
                await pub.publish({"data": idx})
                await asyncio.sleep(0.001)  # without delay, some messages are lost

        end = perf_counter()
        logger.info(f"Message rate: {n_runs/1000/(end-start):.2f} KHz")


if __name__ == "__main__":
    asyncio.run(benchmark())
