#!/usr/bin/env python3
"""
 Example of using ROS_Bridge class to communicate with ROS through rosbridge protocol.

 Copyright (c) 2024 ROX Automation - Jev Kuznetsov
"""
import asyncio
import logging
import random

import coloredlogs  # type: ignore

from roxbot.bridges import ROS_Bridge

coloredlogs.install(level="DEBUG", fmt="%(asctime)s %(levelname)s %(message)s")

logger = logging.getLogger("main")


async def send_messages(bridge: ROS_Bridge):
    """Sends messages to a ROS topic using the ROS bridge."""
    logger.info("Sending messages to ROS topic...")
    async with bridge.publisher("/test_vel", "geometry_msgs/Twist") as pub:
        while True:
            msg = {
                "linear": {"x": round(random.random(), 2), "y": 0.2, "z": 0.0},
                "angular": {"x": 0.0, "y": 0.0, "z": 0.0},
            }
            logger.info(f"Sending message: {msg}")
            await pub.publish(msg)
            await asyncio.sleep(1)


async def main():
    """Main function to demonstrate ROS bridge subscriber and publisher."""

    async with ROS_Bridge("ws://localhost:9090") as bridge:
        await send_messages(bridge)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        logger.info("Keyboard interrupt")
