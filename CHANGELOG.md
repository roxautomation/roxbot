# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.9.0

* added `nodes/gps_node.py`


## 1.8.0

* add robot model protocols
* refactor `DiffDriveModel` **interface change**. could have bumped major version, but there are currently no real dependencies to this.



## 1.7.0

* add `models.DiffDriveModel` - simple two wheeled robot model
* add `odometry.DiffDriveOdometer`


## 1.6.0

* add context manager `async with ROS_Bridge() as bridge`


## 1.5.0

* add `roxbot.bridges.ROS_Bridge` for interfacing to ROS through rosbridge protocol. For examples, see `examples/ros`
* some minor changes in environment- fixed `GPS_REF` value, added mypy config.

## 1.4.0

* Add configuration helper `Config` class
* back to using `GPS_REF` environment variable


## 1.3.3

* `distance_to_line` returns signed number
* added `phi` to line

## 1.1.0

* add `Line` class

## [0.5.0]

### Changed

- internal `Wheel` model now works with `m/s` instead of revolutions

### Added

- dynamic setpoint calculation for driving wheels of `Trike` model.

## [0.4.0] - 2023.07.02

### Changed

- Change `Wheel` acceleration parameter to `m/s2`
- revert to single devcontainer from docker-compose stack.

## [0.3.0] - 2023.06.02

### Added

- `models`, including `Trike`, `Wheel` and `LinearModel`
- `vectors`
- `gps`
