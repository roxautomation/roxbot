#!/bin/bash

# build docker container and attach to it

set -e

IMG_NAME=registry.gitlab.com/roxautomation/roxbot

docker build -t $IMG_NAME .

#docker pull $IMG_NAME

docker run -it --rm \
    -v $(pwd)/..:/workspace \
    -w /workspace \
    $IMG_NAME \
    bash
