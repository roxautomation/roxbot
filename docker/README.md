### Docker

This folder contains docker build files for development.

Development image is built by gitlab ci pipeline. Get it with

`docker pull registry.gitlab.com/roxautomation/roxbot`


Enter container shell with

`start_container_shell.sh`


**note:** Windows users are advised to use WSL to make use of shell scripts.
