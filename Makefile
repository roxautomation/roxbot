.PHONY: install_pkg, clean, container, list, test, uml, serve_docs

SHELL := /bin/bash

UID := $(shell id -u)
GID := $(shell id -g)
USERNAME := $(shell id -u -n)

MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MKFILE_DIR := $(dir $(MKFILE_PATH))

list:
	@cat Makefile

clean:
	rm -rf src/*.egg-info dist venv public public docs/uml


lint:
	pylint -E src
	mypy src


test:
	coverage run -m pytest && coverage report -m

install_pkg:
	@echo "Installing package..."
	pip install .

uml: install_pkg
	# generage uml files
	mkdir -p docs/uml/roxbot
	pyreverse src/roxbot -o png -d docs/uml/roxbot

public: uml
	# build html documentation
	mkdocs build -d public

serve_docs: uml
	# serve html documentation
	mkdocs serve -a 0.0.0.0:8000 -w README.md
