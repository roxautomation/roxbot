#!/usr/bin/env python3
"""
 Prototype of interfacing with ROS through rosbridge

 Copyright (c) 2024 ROX Automation - Jev Kuznetsov
"""

import asyncio
import json

import websockets

PUB_TOPIC = "/bridged"
SUB_TOPIC = "/chatter"
TOPIC_TYPE = (
    "std_msgs/String"  # Change this to the correct ROS message type you are using
)


async def advertise_topic(websocket):
    """Advertise a topic on ROS."""
    advertise_msg = json.dumps(
        {"op": "advertise", "topic": PUB_TOPIC, "type": TOPIC_TYPE}
    )
    await websocket.send(advertise_msg)


async def subscriber(websocket):
    """Subscribe to a topic."""
    print("subscribing")
    subscribe_msg = json.dumps({"op": "subscribe", "topic": SUB_TOPIC})
    await websocket.send(subscribe_msg)

    while True:
        msg = await websocket.recv()  # Receive a message
        print(f"received {msg}")


async def publisher(websocket):
    """publish some test data"""
    print("publishing")
    for idx in range(10):
        msg = json.dumps(
            {"op": "publish", "topic": PUB_TOPIC, "msg": {"data": f"hello {idx}"}}
        )

        print(f"sending {msg}")
        await websocket.send(msg)
        await asyncio.sleep(1)


async def main():
    """Example client connection"""
    async with websockets.connect("ws://localhost:9090") as websocket:
        await advertise_topic(websocket)  # Advertise the topic
        async with asyncio.TaskGroup() as tg:
            sub_task = tg.create_task(subscriber(websocket))
            await tg.create_task(publisher(websocket))
            sub_task.cancel()


asyncio.run(main())
