#!/usr/bin/env python3
"""
 Simple MQTT publisher with asyncio

 produces x,y and t data.

 plot with potjuggler or
 monitor with mosquitto_sub -h localhost -t test_sine -v

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

from typing import Coroutine
import asyncio
import json
import math
import time

import asyncio_mqtt

BROKER = "broker"  # use broker from docker-compose
TOPIC = "test_sine"
FREQ = 30.0  # Hz


def sin_wave(
    t: float, freq: float = 0.5, amp: float = 10.0, offset: float = 0
) -> float:
    """simple sin wave"""
    return amp * math.sin(2 * math.pi * freq * t) + offset


def xy_wave(t: float, freq: float = 0.5, r: float = 10.0):
    """simple sin wave"""
    x = r * math.cos(2 * math.pi * freq * t)
    y = r * math.sin(2 * math.pi * freq * t)
    return round(x, 3), round(y, 3)


async def main():
    """publish messages to broker"""

    t_start = time.time()
    delay = 1 / FREQ

    async with asyncio_mqtt.Client(BROKER) as client:
        idx = 0
        while True:
            t = round(time.time() - t_start, 5)
            x, y = xy_wave(t)
            msg = json.dumps({"t": t, "x": x, "y": y})

            print(f"{msg=}")
            await client.publish(TOPIC, msg)
            idx += 1
            await asyncio.sleep(delay)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("done.")
