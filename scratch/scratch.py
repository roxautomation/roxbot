#!/usr/bin/env python3
"""
 varios code snippets

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

import json

# check tuple packing
dest = "/chatter"
data = {"a": 1, "b": 2}
msg = (dest, data)

# check json

txt = json.dumps(msg)
print(txt)

# check unpacking
dest2, data2 = json.loads(txt)
assert dest2 == dest
assert data2 == data
