# How it works

* Basic building block of roxbot framework is a `Node`. A node runs asyncrhonous processesses and is responsible for a single function.
* Nodes are grouped into subsystems.
* Subsystems are connect by a communication layer, using bridges.

!!! info "The concept of a node is similar to that of ROS2:"
    ROS 2 (Robot Operating System 2) nodes are the fundamental building blocks within the ROS 2 environment. They are entities that encapsulate a single functionality or a collection of related functionalities, and communicate with other nodes within the system. Nodes use a publish/subscribe messaging model to share data and handle events, making it easier to design modular and scalable robotic systems.



* Nodes do stuff and should have one purpose/responsibility (ex. GPS nodes provide lat and long coord, etc)
* Nodes communicate and interact asynchronously
* Nodes communicate with each other "inputs" and an "outputs", where they can be either a queue, or a callback.
* Nodes can be grouped into a `subsystem`. Each subsystem runs in a Docker container.
* Subsystems and external systems are connected with _bridges_.
    - `MQTT_Bridge` is a good choice to enable message broadcasting.
    -  `WS_bridge` is a good choice for point-to-point connections, like interfacing to web interfaces.
    - `ROS_Bridge` can be used to connect to a ROS2 subsystem.


!!! note
    An `input` can be connected to only *one* output. One `output` can have multiple inputs
    connected to it (run multiple callbacks for example)


## Example robot system

Below is an example system that consists of multiple subsystems.


```mermaid
graph TD

    classDef yellow fill:#ffe74c;
    classDef red fill:#ff5964;
    classDef white fill:#ffffff,stroke:#000000;
    classDef green fill:#6bf178;
    classDef blue fill:#35a7ff;
    classDef lightGray fill:#d3d3d3

    %% core container
    subgraph motion_subsystem
        localisatioin_node -->|"(x,y,phi)"|follower_node
        follower_node -->|"(v,r)"|kinematics_node
        joystick_node --> |"(v,r)"|kinematics_node

        kinematics_node -->|setpoint|left_wheel_node -->|position|odometry_node
        kinematics_node -->|setpoint|right_wheel_node -->|position|odometry_node
        kinematics_node -->|setpoint|steering_wheel_node -->|position|odometry_node

        odometry_node -->|"(x,y,phi)"|localisatioin_node
    end

    subgraph logging_subsystem
        influx_node
        remote_logging_node
    end

    subgraph safety_subsystem
        estop_node
        lidar_node
    end


    %% connections
    gps -->|"serial (nmea)"| localisatioin_node
    web_joystick -->|websocket|joystick_node

    %% styling
    class ws_bridge,ws_bridge2 green
    class gps blue

    logging_subsystem---|bridge|motion_subsystem
    safety_subsystem---|bridge|motion_subsystem

```

!!! note
    Most hardware comonents have been left out in the schematic above. One that is
    shown is the GPS in blue. Other hardware components are connected to their controlling nodes
    in a similar way, so a `left_wheel_node` controls a left wheel servo contoller.
