## Development

**Please develop inside the container**, this will ensure all the required checks (`pylint` & `mypy`) as well as formatting (`black`)

If you are not familiar with devcontainers, read [Developing inside a Container](https://code.visualstudio.com/docs/devcontainers/containers) first

1. Clone this repository
2. open dir in *VS Code* `vscode .`
3. rebuild and reopen in container (you'll need `Dev Containers` extension)

**note**: if a container with `devcontainer` name already exists, an error will occur. You can remove it with
`docker container prune -f`


### Contribution guideline

**For significantly large changes & features**
1. Create an issue describing issue.
2. Create a Merge Request to `alpha` branch. This will automatically create a feature branch.
3. When feature is finished, set status to "ready" and submit for review.

**For small changes**

`alpha` is the *continuous integration* branch. It may be used to directly add functionality that is sufficiently small.

"sufficiently small" = does not break or significantly change existing functionality.




### What goes where

* `gitlab-ci.yml` - gitlab ci script
* `.devcontainer/requirements.txt` packages required for development
* `setup.py` - main packge setup file (cli scripts, dependencies etc.)
* `docs` - documentation, uses mkdocs
* `docker` - devcontainer files
* `install` - scripts for preparing host system

--8<-- "docker/README.md"

### Version control

use `bumpversion major/minor/patch` to update version number.

### Documentation

The documentation is automatically generated from the content of the [docs directory](./docs) and from the docstrings
 of the public signatures of the source code.

run `serve_docs.sh` on host system to build and serve documentation.

**note:** `pyreverse` creates images of packages and classes in `docs/uml/..`

### Pre-commit

optional. Add `precommit install` to `init_container.sh` if required.

This project was forked from [cookiecutter template](https://gitlab.com/sjev/python-template) template.
